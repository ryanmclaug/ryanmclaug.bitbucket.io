var 305_lab0xFF =
[
    [ "Week 1: Starting a User Interface (UI) and Sending Arbitrary Data", "week1_305.html", [
      [ "Front End: User Interface Running in Python", "week1_305.html#frontEnd", null ],
      [ "UI Task: Complement to Front End Running on Nucleo", "week1_305.html#uiTask", null ],
      [ "Week 1 Deliverables", "week1_305.html#deliverables1", null ]
    ] ],
    [ "Week 2: Interfacing with Encoders", "week2_305.html", [
      [ "Encoder Driver", "week2_305.html#encDriver", null ],
      [ "Encoder Task", "week2_305.html#encTask", null ],
      [ "Sharing Data Between Tasks", "week2_305.html#sharedData", null ],
      [ "Changes to the UI Task Finite State Machine", "week2_305.html#uiUpdates", null ]
    ] ],
    [ "Week 3: Interfacing with DC Motors and Implemenation of a Controller", "week3_305.html", [
      [ "Motor Driver", "week3_305.html#motorDriver", null ],
      [ "Controller Driver", "week3_305.html#controllerDriver", null ],
      [ "Controller Task", "week3_305.html#controllerTask", null ],
      [ "Week 3 Deliverables", "week3_305.html#deliverables3", null ]
    ] ],
    [ "Week 4: Switching from Setpoint Tracking to Refrence Tracking", "week4_305.html", [
      [ "Updates to the Front End (PC script)", "week4_305.html#frontEndChanges", null ],
      [ "Updates to the Controller Task", "week4_305.html#controllerTaskChanges", null ],
      [ "Final Task Diagram", "week4_305.html#finalTaskDiagram", null ],
      [ "Final Results", "week4_305.html#finalResults", null ]
    ] ]
];