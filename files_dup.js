var files_dup =
[
    [ "305_Lab0x01_mainPage.py", "305__Lab0x01__mainPage_8py.html", null ],
    [ "305_Lab0x02_mainPage.py", "305__Lab0x02__mainPage_8py.html", null ],
    [ "305_Lab0x03_mainPage.py", "305__Lab0x03__mainPage_8py.html", null ],
    [ "305_Lab0xFF_mainPage.py", "305__Lab0xFF__mainPage_8py.html", null ],
    [ "405_Lab0x01_mainPage.py", "405__Lab0x01__mainPage_8py.html", null ],
    [ "405_Lab0x02_mainPage.py", "405__Lab0x02__mainPage_8py.html", null ],
    [ "405_Lab0x03_mainPage.py", "405__Lab0x03__mainPage_8py.html", null ],
    [ "405_Lab0x04_mainPage.py", "405__Lab0x04__mainPage_8py.html", null ],
    [ "405_Lab0xFF_Page.py", "405__Lab0xFF__Page_8py.html", null ],
    [ "507_statTracker_Page.py", "507__statTracker__Page_8py.html", null ],
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "controllerTask.py", "controllerTask_8py.html", [
      [ "controllerTask", "classcontrollerTask_1_1controllerTask.html", "classcontrollerTask_1_1controllerTask" ]
    ] ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "ctrlTask.py", "ctrlTask_8py.html", [
      [ "ctrlTask", "classctrlTask_1_1ctrlTask.html", "classctrlTask_1_1ctrlTask" ]
    ] ],
    [ "dataTask.py", "dataTask_8py.html", "dataTask_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder", "classencoder_1_1encoder.html", "classencoder_1_1encoder" ]
    ] ],
    [ "encoderDriver.py", "encoderDriver_8py.html", [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", "encoderTask_8py" ],
    [ "Fibonacci.py", "Fibonacci_8py.html", "Fibonacci_8py" ],
    [ "frontEnd.py", "frontEnd_8py.html", "frontEnd_8py" ],
    [ "game.py", "game_8py.html", [
      [ "game", "classgame_1_1game.html", "classgame_1_1game" ]
    ] ],
    [ "Lab0x02_A.py", "Lab0x02__A_8py.html", "Lab0x02__A_8py" ],
    [ "Lab0x02_B.py", "Lab0x02__B_8py.html", "Lab0x02__B_8py" ],
    [ "Lab0x03_PC_Frontend.py", "Lab0x03__PC__Frontend_8py.html", "Lab0x03__PC__Frontend_8py" ],
    [ "Lab0x03_uiTask.py", "Lab0x03__uiTask_8py.html", "Lab0x03__uiTask_8py" ],
    [ "ledpwm.py", "ledpwm_8py.html", "ledpwm_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mathPuzzle.py", "mathPuzzle_8py.html", "mathPuzzle_8py" ],
    [ "mathPuzzle_mainPage.py", "mathPuzzle__mainPage_8py.html", null ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "motorDriver.py", "motorDriver_8py.html", [
      [ "motorDriver", "classmotorDriver_1_1motorDriver.html", "classmotorDriver_1_1motorDriver" ]
    ] ],
    [ "motorDriverChannel.py", "motorDriverChannel_8py.html", [
      [ "motorDriverChannel", "classmotorDriverChannel_1_1motorDriverChannel.html", "classmotorDriverChannel_1_1motorDriverChannel" ]
    ] ],
    [ "motorTask.py", "motorTask_8py.html", [
      [ "motorTask", "classmotorTask_1_1motorTask.html", "classmotorTask_1_1motorTask" ]
    ] ],
    [ "PCFrontEnd.py", "PCFrontEnd_8py.html", "PCFrontEnd_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "stat_tracker.cpp", "stat__tracker_8cpp.html", null ],
    [ "stat_tracker.h", "stat__tracker_8h.html", [
      [ "StatTracker", "classStatTracker.html", "classStatTracker" ]
    ] ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "touchPanelCalibration_main.py", "touchPanelCalibration__main_8py.html", "touchPanelCalibration__main_8py" ],
    [ "touchPanelDriver.py", "touchPanelDriver_8py.html", [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "touchPanelTask.py", "touchPanelTask_8py.html", [
      [ "touchPanelTask", "classtouchPanelTask_1_1touchPanelTask.html", "classtouchPanelTask_1_1touchPanelTask" ]
    ] ],
    [ "uiTask.py", "uiTask_8py.html", "uiTask_8py" ],
    [ "vendotron.py", "vendotron_8py.html", "vendotron_8py" ]
];