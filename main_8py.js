var main_8py =
[
    [ "ctrlTask", "main_8py.html#a30c79a6315dcc2a1b1902c59c1de518e", null ],
    [ "ctrlTaskObj", "main_8py.html#a8a82a3ed09bd8db0c3bf754f36c59837", null ],
    [ "dataColTask", "main_8py.html#a72a9b5184388cc98eb35f1bd6f85c496", null ],
    [ "encoderTaskObj", "main_8py.html#af647b1fe20bad55da2c241f73c672286", null ],
    [ "encTask", "main_8py.html#aea8952b5afc8c1b84c8db14cddd3e249", null ],
    [ "kxMatrix", "main_8py.html#ad16fc39f01e44c0a539f39ad51326630", null ],
    [ "kyMatrix", "main_8py.html#a93c922a622bcee19cbc3e40f8e7e25bf", null ],
    [ "motorTaskObj", "main_8py.html#a3c4c980529da66ba0fc1558fbd76f597", null ],
    [ "motTask", "main_8py.html#afd61d5b7c9bd88a44bd7b2bc291e42f1", null ],
    [ "myuart", "main_8py.html#a9be3fc12d7dd7ca0504e230b84d96936", null ],
    [ "periods", "main_8py.html#ac62c248b37fa38677883dc5f09f0a43d", null ],
    [ "tchPanelTask", "main_8py.html#afc9295617708a950f85057d45da3e70c", null ],
    [ "touchPanelTaskObj", "main_8py.html#a13a212aa771bec8fe4fb3d39236f0e57", null ],
    [ "uiTask", "main_8py.html#a5a7317122c4805af84f2a95dc3ed815a", null ],
    [ "uiTaskObj", "main_8py.html#ac106242be880198986abe6aaaffde8ad", null ]
];