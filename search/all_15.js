var searchData=
[
  ['ready_274',['ready',['../classcotask_1_1Task.html#a6102bc35d7cb1ce292abc85d4ddc23e1',1,'cotask::Task']]],
  ['realperiod_275',['realPeriod',['../classencoder_1_1encoder.html#add9cb64deb4abc99e2ce3ad250f54dbb',1,'encoder::encoder']]],
  ['reset_276',['reset',['../classgame_1_1game.html#a8f946344bcb6c8c64c175afaa90bef1b',1,'game::game']]],
  ['reset_5fprofile_277',['reset_profile',['../classcotask_1_1Task.html#a1bcbfa7dd7086112af20b7247ffa4a2e',1,'cotask::Task']]],
  ['restarttime_278',['restartTime',['../classgame_1_1game.html#ae74d04558f59e87b1f2536e8fb189f68',1,'game::game']]],
  ['results_279',['results',['../touchPanelCalibration__main_8py.html#a7b480f40bb3b595456733bdf457065a0',1,'touchPanelCalibration_main']]],
  ['round_280',['round',['../classgame_1_1game.html#aa3b197b514741951bd0c3c3d6842e41b',1,'game::game']]],
  ['rr_5fsched_281',['rr_sched',['../classcotask_1_1TaskList.html#a01614098aedc87b465d5525c6ccb47ce',1,'cotask::TaskList']]],
  ['run_282',['run',['../classcontrollerTask_1_1controllerTask.html#a3608ba9f1a2c3a128c097b24c4069f22',1,'controllerTask.controllerTask.run()'],['../classgame_1_1game.html#a91d96b777404456767fe1223dddab6c8',1,'game.game.run()'],['../classLab0x03__uiTask_1_1uiTask.html#ac592306ee36b5a4fd3c42004409cf717',1,'Lab0x03_uiTask.uiTask.run()'],['../classmcp9808_1_1mcp9808.html#a4bad956401e66bf0a8a94a1be57596c6',1,'mcp9808.mcp9808.run()']]],
  ['runmain_283',['runMain',['../classmcp9808_1_1mcp9808.html#ad83ede69ee91b55019dbb2cbba9114e8',1,'mcp9808::mcp9808']]],
  ['rxntimes_284',['rxnTimes',['../Lab0x02__A_8py.html#aae6947c9ff67ba1d7d888550ce69cc3b',1,'Lab0x02_A.rxnTimes()'],['../Lab0x02__B_8py.html#abdfb9a6057164c2e5572dc9c0960722e',1,'Lab0x02_B.rxnTimes()']]],
  ['resistive_20touch_20panel_3a_20locating_20the_20ball_285',['Resistive Touch Panel: Locating the Ball',['../touchPanelPage405.html',1,'405_lab0xFF']]]
];
