var searchData=
[
  ['adc_22',['adc',['../classLab0x03__uiTask_1_1uiTask.html#a5cae0f14c3b9539e53054ebe6ad9efd3',1,'Lab0x03_uiTask.uiTask.adc()'],['../classmcp9808_1_1mcp9808.html#a2e82edd7de4f57c0438c13ae5a03d593',1,'mcp9808.mcp9808.adc()']]],
  ['add_5fdata_23',['add_data',['../classStatTracker.html#ae67969a721b4a4ff38cf9fa16928b281',1,'StatTracker::add_data(float newData_f)'],['../classStatTracker.html#a24dad0ccb80e777a48d6e31a4a2f0c08',1,'StatTracker::add_data(int32_t newData_32s)'],['../classStatTracker.html#ad50a6db634f3bd3dd833d4f06b4eddb6',1,'StatTracker::add_data(uint32_t newData_32u)']]],
  ['addr_24',['addr',['../classmcp9808_1_1mcp9808.html#ad2aac0f4db29a3a742a01fff60614a73',1,'mcp9808::mcp9808']]],
  ['allpositions_25',['allPositions',['../classtouchPanelDriver_1_1touchPanelDriver.html#adbc3122437b49d4908eb57a22c4e224d',1,'touchPanelDriver::touchPanelDriver']]],
  ['ans_26',['ans',['../mathPuzzle_8py.html#a459fd49781d997c70d4f6a427c7df973',1,'mathPuzzle']]],
  ['ansstring_27',['ansString',['../mathPuzzle_8py.html#ae3aa5bbb4308766e4c05a3b87c4339e8',1,'mathPuzzle']]],
  ['any_28',['any',['../classtask__share_1_1Queue.html#a7cb2d23978b90a232cf9cea4cc0ccb6b',1,'task_share::Queue']]],
  ['append_29',['append',['../classcotask_1_1TaskList.html#aa690015d692390e17cb777ff367ae159',1,'cotask::TaskList']]],
  ['average_30',['average',['../classStatTracker.html#a8c0d201e498e31cbdf42e21cac5200e8',1,'StatTracker']]],
  ['avgrxntime_31',['avgRxnTime',['../Lab0x02__A_8py.html#aef03b3e8d97e280c40108f015afb204a',1,'Lab0x02_A.avgRxnTime()'],['../Lab0x02__B_8py.html#a10daa28a55ecf51e5fe4998d8d67ac61',1,'Lab0x02_B.avgRxnTime()']]]
];
