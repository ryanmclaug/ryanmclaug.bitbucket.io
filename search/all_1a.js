var searchData=
[
  ['week_201_3a_20starting_20a_20user_20interface_20_28ui_29_20and_20sending_20arbitrary_20data_408',['Week 1: Starting a User Interface (UI) and Sending Arbitrary Data',['../week1_305.html',1,'305_lab0xFF']]],
  ['week_202_3a_20interfacing_20with_20encoders_409',['Week 2: Interfacing with Encoders',['../week2_305.html',1,'305_lab0xFF']]],
  ['week_203_3a_20interfacing_20with_20dc_20motors_20and_20implemenation_20of_20a_20controller_410',['Week 3: Interfacing with DC Motors and Implemenation of a Controller',['../week3_305.html',1,'305_lab0xFF']]],
  ['week_204_3a_20switching_20from_20setpoint_20tracking_20to_20refrence_20tracking_411',['Week 4: Switching from Setpoint Tracking to Refrence Tracking',['../week4_305.html',1,'305_lab0xFF']]],
  ['wintime_412',['winTime',['../classgame_1_1game.html#a32b082b9bdc6e650b0734959d52c4422',1,'game::game']]],
  ['writecsv_413',['writeCSV',['../frontEnd_8py.html#a1f57a39163d97fdfe995670dfafedc50',1,'frontEnd']]],
  ['writetocsv_414',['writeToCSV',['../Lab0x03__PC__Frontend_8py.html#a20b55d29dabe14d2be4b9e65074117b9',1,'Lab0x03_PC_Frontend.writeToCSV()'],['../PCFrontEnd_8py.html#a69b92718b062ce5eca27eab47116f9a6',1,'PCFrontEnd.writeToCSV()']]]
];
