var ME405_Lab0xFF =
[
    [ "System Modeling: Kinetics and Kinematics", "HW0x02.html", [
      [ "System Definition", "HW0x02.html#HW0x02Intro", null ],
      [ "System Kinematics", "HW0x02.html#HW0x02Kinematics", null ],
      [ "System Kinetics", "HW0x02.html#HW0x02Kinetics", null ]
    ] ],
    [ "Linearization EOMs and Simulating Closed-Loop Control", "HW0x04.html", [
      [ "Uncoupling and Linearizing EOMs", "HW0x04.html#HW0x04Linearization", null ],
      [ "Simulating the Motion of the System", "HW0x04.html#HW0x04Simulation", [
        [ "Open Loop Response", "HW0x04.html#HW0x04OpenLoop", null ],
        [ "Closed Loop Response", "HW0x04.html#HW0x04ClosedLoop", null ]
      ] ]
    ] ],
    [ "Full-State Feedback", "HW0x05.html", [
      [ "Deriving Pole Locations", "HW0x05.html#HW0x05PoleSelection", null ],
      [ "Results", "HW0x05.html#HW0x05Results", null ]
    ] ]
];