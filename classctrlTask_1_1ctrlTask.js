var classctrlTask_1_1ctrlTask =
[
    [ "__init__", "classctrlTask_1_1ctrlTask.html#adca260ccb8e69d941f6bc8a333d25947", null ],
    [ "ctrlTaskFcn", "classctrlTask_1_1ctrlTask.html#a01aa90fdcac3a79d414ddbd1b8d63bd5", null ],
    [ "transitionTo", "classctrlTask_1_1ctrlTask.html#a153e5ce9eb3c8b594356bbad5b1a1f43", null ],
    [ "dataToSave", "classctrlTask_1_1ctrlTask.html#a47c37f47dd18d289630bc8b1ca33c0f1", null ],
    [ "debugFlag", "classctrlTask_1_1ctrlTask.html#a7b219206edbab2faae7789a6de7919bc", null ],
    [ "kMatrixX", "classctrlTask_1_1ctrlTask.html#a0ae30991604bbda3cecdb39968ba27a2", null ],
    [ "kMatrixY", "classctrlTask_1_1ctrlTask.html#a9785acb83a1fb4f846e1efb6c975ea68", null ],
    [ "kPrimeFactor", "classctrlTask_1_1ctrlTask.html#a93a5023c84f6665f74110cdedff12628", null ],
    [ "xThresh", "classctrlTask_1_1ctrlTask.html#a7755eaad5471d51bd21b6767186dfbeb", null ],
    [ "yThresh", "classctrlTask_1_1ctrlTask.html#ab986c58297afbde46478f693ea7567b1", null ]
];