/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ryan McLaughlin | Mechatronics Portfolio", "index.html", [
    [ "Table of Contents", "index.html", [
      [ "ME 305: Introduction to Mechatronics", "index.html#me305", null ],
      [ "ME 405: Mechatronics", "index.html#me405", null ],
      [ "Additional Code Examples", "index.html#addtlCode", null ]
    ] ],
    [ "305 Lab 0x01: Fibonacci Calculator", "305_lab0x01.html", null ],
    [ "305 Lab 0x02: Getting Started With Hardware", "305_lab0x02.html", null ],
    [ "305 Lab 0x03: Simon Says", "305_lab0x03.html", null ],
    [ "305 Lab 0xFF: Final Project", "305_lab0xFF.html", "305_lab0xFF" ],
    [ "405 Lab 0x01: Vendotron", "405_lab0x01.html", null ],
    [ "405 Lab 0x02: Reaction Times", "405_lab0x02.html", [
      [ "Part A: Software Based Methodology", "405_lab0x02.html#partA", null ],
      [ "Part B: Hardware Based Methodology", "405_lab0x02.html#partB", null ],
      [ "Demonstration Videos", "405_lab0x02.html#lab2_demoVideos", null ],
      [ "Discussion Questions", "405_lab0x02.html#lab2_discussionQuestions", null ]
    ] ],
    [ "405 Lab 0x03: Pushing the Right Buttons", "405_lab0x03.html", [
      [ "Project Description:", "405_lab0x03.html#description", null ]
    ] ],
    [ "405 Lab 0x04: Hot or Not?", "405_lab0x04.html", [
      [ "Lab Description:", "405_lab0x04.html#description04", null ],
      [ "Code Implementation", "405_lab0x04.html#codeLab0x04", null ]
    ] ],
    [ "405 Lab 0xFF: Final Project", "405_lab0xFF.html", "405_lab0xFF" ],
    [ "507: Statistically Significant", "507_statTracker.html", null ],
    [ "mathPuzzle", "mathPuzzle.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"305__Lab0x01__mainPage_8py.html",
"classencoder_1_1encoder.html#a4d7b005b2b6976224be2f7d8a71ffd1e",
"functions_vars.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';