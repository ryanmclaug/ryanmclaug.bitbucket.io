var annotated_dup =
[
    [ "closedLoop", null, [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "controllerTask", null, [
      [ "controllerTask", "classcontrollerTask_1_1controllerTask.html", "classcontrollerTask_1_1controllerTask" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "ctrlTask", null, [
      [ "ctrlTask", "classctrlTask_1_1ctrlTask.html", "classctrlTask_1_1ctrlTask" ]
    ] ],
    [ "dataTask", null, [
      [ "dataTask", "classdataTask_1_1dataTask.html", "classdataTask_1_1dataTask" ]
    ] ],
    [ "encoder", null, [
      [ "encoder", "classencoder_1_1encoder.html", "classencoder_1_1encoder" ]
    ] ],
    [ "encoderDriver", null, [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "encoderTask", null, [
      [ "encoderTask", "classencoderTask_1_1encoderTask.html", "classencoderTask_1_1encoderTask" ]
    ] ],
    [ "game", null, [
      [ "game", "classgame_1_1game.html", "classgame_1_1game" ]
    ] ],
    [ "Lab0x03_uiTask", null, [
      [ "uiTask", "classLab0x03__uiTask_1_1uiTask.html", "classLab0x03__uiTask_1_1uiTask" ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "motorDriver", null, [
      [ "motorDriver", "classmotorDriver_1_1motorDriver.html", "classmotorDriver_1_1motorDriver" ]
    ] ],
    [ "motorDriverChannel", null, [
      [ "motorDriverChannel", "classmotorDriverChannel_1_1motorDriverChannel.html", "classmotorDriverChannel_1_1motorDriverChannel" ]
    ] ],
    [ "motorTask", null, [
      [ "motorTask", "classmotorTask_1_1motorTask.html", "classmotorTask_1_1motorTask" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "touchPanelDriver", null, [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "touchPanelTask", null, [
      [ "touchPanelTask", "classtouchPanelTask_1_1touchPanelTask.html", "classtouchPanelTask_1_1touchPanelTask" ]
    ] ],
    [ "uiTask", null, [
      [ "uiTask", "classuiTask_1_1uiTask.html", "classuiTask_1_1uiTask" ]
    ] ],
    [ "StatTracker", "classStatTracker.html", "classStatTracker" ]
];