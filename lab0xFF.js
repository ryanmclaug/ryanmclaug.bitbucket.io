var lab0xFF =
[
    [ "Week 1: Starting a User Interface (UI) and Sending Arbitrary Data", "week1.html", [
      [ "Front End: User Interface Running in Python", "week1.html#frontEnd", null ],
      [ "UI Task: Complement to Front End Running on Nucleo", "week1.html#uiTask", null ],
      [ "Week 1 Deliverables", "week1.html#deliverables1", null ]
    ] ],
    [ "Week 2: Interfacing with Encoders", "week2.html", [
      [ "Encoder Driver", "week2.html#encDriver", null ],
      [ "Encoder Task", "week2.html#encTask", null ],
      [ "Sharing Data Between Tasks", "week2.html#sharedData", null ],
      [ "Changes to the UI Task Finite State Machine", "week2.html#uiUpdates", null ]
    ] ],
    [ "Week 3: Interfacing with DC Motors and Implemenation of a Controller", "week3.html", [
      [ "Motor Driver", "week3.html#motorDriver", null ],
      [ "Controller Driver", "week3.html#controllerDriver", null ],
      [ "Controller Task", "week3.html#controllerTask", null ],
      [ "Week 3 Deliverables", "week3.html#deliverables3", null ]
    ] ],
    [ "Week 4: Switching from Setpoint Tracking to Refrence Tracking", "week4.html", [
      [ "Updates to the Front End (PC script)", "week4.html#frontEndChanges", null ],
      [ "Updates to the Controller Task", "week4.html#controllerTaskChanges", null ],
      [ "Final Task Diagram", "week4.html#finalTaskDiagram", null ],
      [ "Final Results", "week4.html#finalResults", null ]
    ] ]
];