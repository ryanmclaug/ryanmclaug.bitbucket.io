var classLab0x03__uiTask_1_1uiTask =
[
    [ "__init__", "classLab0x03__uiTask_1_1uiTask.html#a3dbad38ae285fac90bc1430ab7337cfa", null ],
    [ "run", "classLab0x03__uiTask_1_1uiTask.html#ac592306ee36b5a4fd3c42004409cf717", null ],
    [ "transitionTo", "classLab0x03__uiTask_1_1uiTask.html#ae272d6ef640eacfe2318f2463e463e3e", null ],
    [ "adc", "classLab0x03__uiTask_1_1uiTask.html#a5cae0f14c3b9539e53054ebe6ad9efd3", null ],
    [ "debugFlag", "classLab0x03__uiTask_1_1uiTask.html#ad74f9565835d3666a94d792478ab76aa", null ],
    [ "differenceInVoltage", "classLab0x03__uiTask_1_1uiTask.html#a7fa48b5d3d9d9fe2fb17827765d23dc8", null ],
    [ "n", "classLab0x03__uiTask_1_1uiTask.html#a7556f34be9e1a330d67acafc48ac16f9", null ],
    [ "sendingDataFlag", "classLab0x03__uiTask_1_1uiTask.html#af9061297666ba45d5ee6ac00154feda6", null ],
    [ "state", "classLab0x03__uiTask_1_1uiTask.html#a98ca6c3795acb18e78809b9f7274ab0c", null ],
    [ "tim", "classLab0x03__uiTask_1_1uiTask.html#a93b7eda7197913c51d5a0687ca226e4e", null ],
    [ "val", "classLab0x03__uiTask_1_1uiTask.html#ad80de120aba4f4a117a899aba0f3015f", null ],
    [ "voltages", "classLab0x03__uiTask_1_1uiTask.html#ad92441f01fa45692a4c45fa8b6495a7e", null ]
];